<?php  include('../config.php'); ?>
<?php  include(ROOT_PATH . '/admin/includes/admin_functions.php'); ?>
<?php
// Get all admin users from DB
$admins = getAdminUsers();
$roles = ['Admin', 'Author'];
?>
<?php include(ROOT_PATH . '/admin/includes/head_section.php'); ?>
<title>Admin </title>
</head>
<body>
<!-- admin navbar -->
<?php include(ROOT_PATH . '/admin/includes/navbar.php') ?>
<div class="container content">
    <!-- Left menu -->
    <?php include(ROOT_PATH . '/admin/includes/menu.php') ?>

    <div class="action">
        <h1 class="page-title">Create/Edit Admin User</h1>

        <form method="post" action="<?php echo BASE_URL . 'admin/users.php'; ?>" >

            <!-- validation errors for the form -->
            <?php include(ROOT_PATH . '/includes/errors.php') ?>

            <!-- if editing user, l'identifiant est nécessaire pour identifier cet utilisateur -->
            <?php if ($isEditingUser === true): ?>
                <input type="hidden" name="admin_id" value="<?php echo $admin_id; ?>">
            <?php endif ?>

            <input type="text" name="username" value="<?php echo $username; ?>" placeholder="pseudo">
            <input type="email" name="email" value="<?php echo $email ?>" placeholder="Email">
            <input type="password" name="password" placeholder="mot de passe">
            <input type="password" name="passwordConfirmation" placeholder="confirmation  mot de passe">
            <select name="role">
                <option value="" sélèctionner désactiver>attribuer un rôle</option>
                <?php foreach ($roles as $key => $role): ?>
                    <option value="<?php echo $role; ?>"><?php echo $role; ?></option>
                <?php endforeach ?>
            </select>

            <!-- afficher le bouton de mise à jour au lieu du bouton de création
            <?php if ($isEditingUser === true): ?>
                <button type="submit" class="btn" name="update_admin">METTRE A JOUR</button>
            <?php else: ?>
                <button type="submit" class="btn" name="create_admin">enregistrer l'utilisateur</button>
            <?php endif ?>
        </form>
    </div>
    <!-- // Middle form - to create and edit -->

    <!-- Display records from DB-->
    <div class="table-div">
        <!-- Display notification message -->
        <?php include(ROOT_PATH . '/includes/messages.php') ?>

        <?php if (empty($admins)): ?>
            <h1>No admins in the database.</h1>
        <?php else: ?>
            <table class="table">
                <thead>
                <th>N</th>
                <th>Admin</th>
                <th>Role</th>
                <th colspan="2">Action</th>
                </thead>
                <tbody>
                <?php foreach ($admins as $key => $admin): ?>
                    <tr>
                        <td><?php echo $key + 1; ?></td>
                        <td>
                            <?php echo $admin['username']; ?>, &nbsp;
                            <?php echo $admin['email']; ?>
                        </td>
                        <td><?php echo $admin['role']; ?></td>
                        <td>
                            <a class="fa fa-pencil btn edit"
                               href="users.php?edit-admin=<?php echo $admin['id'] ?>">
                            </a>
                        </td>
                        <td>
                            <a class="fa fa-trash btn delete"
                               href="users.php?delete-admin=<?php echo $admin['id'] ?>">
                            </a>
                        </td>
                    </tr>
                <?php endforeach ?>
                </tbody>
            </table>
        <?php endif ?>
    </div>
    <!-- // Display records from DB -->
</div>
</body>
</html>
