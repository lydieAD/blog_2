<?php  include('config.php'); ?>
<!-- Source code for handling registration and login -->
<?php  include('includes/registration_login.php'); ?>

<?php include('includes/head_section.php'); ?>

<title>LifeBlog | Sign up </title>
</head>
<body>
<div class="container">
    <!-- Navbar -->
    <?php include( ROOT_PATH . '/includes/navbar.php'); ?>
    <!-- // Navbar -->

    <div style="width: 40%; margin: 20px auto;">
        <form method="post" action="register.php" >
            <h2>s'inscrire sur blog_2</h2>
            <?php include(ROOT_PATH . '/includes/errors.php') ?>
            <input  type="text" name="username" value="<?php echo $username; ?>"  placeholder="pseudo">
            <input type="email" name="email" value="<?php echo $email ?>" placeholder="Email">
            <input type="password" name="password_1" placeholder="mot de passe">
            <input type="password" name="password_2" placeholder="confirmation de mot de passe">
            <button type="submit" class="btn" name="reg_user">enregistrer</button>
            <p>
               deja inscrit? <a href="login.php">s'identifier</a>
            </p>
        </form>
    </div>
</div>
<!-- // container -->
<!-- Footer -->
<?php include( ROOT_PATH . '/includes/footer.php'); ?>
<!-- // Footer -->
