<?php if (isset($_SESSION['user']['username'])) { ?>
    <div class="logged_in_info">
        <span>bienvenue <?php echo $_SESSION['user']['username'] ?></span>
        |
        <span><a href="logout.php">déconnexion</a></span>
    </div>
<?php }else{ ?>
    <div class="banner">
        <div class="welcome_msg">
            <h1>inspiration du jour</h1>
            <p>
                remettre <br>
               sa vie sur les  <br>
                bonnes rails <br>
                <span>~ Blandine</span>
            </p>
            <a href="register.php" class="btn">Nous rejoindre</a>
        </div>

        <div class="login_div">
            <form action="<?php echo BASE_URL . 'index.php'; ?>" method="post" >
                <h2>s'identifier</h2>
                <div style="width: 60%; margin: 0px auto;">
                    <?php include(ROOT_PATH . '/includes/errors.php') ?>
                </div>
                <input type="text" name="username" value="<?php echo $username; ?>" placeholder="Username">
                <input type="password" name="password"  placeholder="Password">
                <button class="btn" type="submit" name="login_btn">s'incrire</button>
            </form>
        </div>
    </div>
<?php } ?>